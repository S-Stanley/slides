# Manifest

### Master your time

With a chronometre/counter already visible by you, you have the ability
to master your time and your presentation. Allowing you to spend more or less
time on some part of your presentation.

You can decide to hide it for the audience, or disabled it completely if you
want to.

### Max quantity of text

Great presentation are made with picture and few sentence. We are helping you
manage this by preventing you from writing to much text in each slides.

### Each presentation is unique

You can differentiate yourself easily from others with multiple templates
available. Each one unique from the other.

### Everything you need is alreayd there

Great presentation mostly include icons, we have setup them for you right away,
in the tools directly so you don't have to go anywere else to find it.

### You will not forget your speach

In the presentator view, you can have some textual element allowing you to
see somes textes, and help you in case your forgot what you have to say.

### Remote control

With your smartphone, you can control easily the presentation. Changing slide,
adding somes sounds, or see your notes for each slides.